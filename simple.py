from multipledispatch import dispatch
from dialogue_system import DialogueSystem
from datastructs.assignment import Assignment
from modules.simulation.simulator import Simulator
from readers.xml_domain_reader import XMLDomainReader
from bn.distribs.distribution_builder import CategoricalTableBuilder
from dialogue_state import DialogueState
from modules.module import Module
from modules.simulation.reward_learner import RewardLearner
from collections import Collection
from bn.values.none_val import NoneVal
from utils.xml_utils import XMLUtils

import time



class SimpleModule(Module): # extends OpenDial's Module class

    def __init__(self):
        self._paused = True

    def start(self):
        """
        Starts the module.
        """
        self._paused = False
        self._action = None
        self._wac_action = None

    @dispatch(DialogueState, Collection)
    def trigger(self, state, update_vars):
        if 'action' in update_vars and state.has_chance_node('action'):
            action = str(state.query_prob('action').get_best())
            print('action={}'.format(action))
            self._action = action

        if 'wac_action' in update_vars and state.has_chance_node('wac_action'):
            action = str(state.query_prob('wac_action').get_best())
            print('wac_action={}'.format(action))
            self._wac_action = action
            #print(str(state.query_prob('objectInView').get_best()))

    @dispatch(bool)
    def pause(self, to_pause):
        """
        Pauses the module.

        :param to_pause: whether to pause the module or not
        """
        self._paused = to_pause

    def is_running(self):
        """
        Returns whether the module is currently running or not.

        :return: whether the module is running or not.
        """
        return not self._paused





domain_dir = 'domains/cozmo/cozmo.xml'

# setup
system = DialogueSystem()
system.change_domain(XMLDomainReader.extract_domain(domain_dir))
system.change_settings(system.get_settings())
mod = SimpleModule()
system.attach_module(mod) # modules can read the dialogue state
system.attach_module(RewardLearner(system)) 
system.start_system()

# simulate state updates
# time.sleep(2)
system.add_content(Assignment('object_in_view','true'))
# time.sleep(3)
system.add_content(Assignment('near_object','true'))

# simluate state updates with learning/updating parameters

for i in range(3):
    print('update 1')
    system.add_content(Assignment('top_word_num_seen_examples',2.0))
    system.add_content(Assignment('top_word_visible_object_prob',0.1))
    reward = 1 if mod._wac_action != 'wait' else -5
    system.get_state().add_evidence(Assignment('R(wac_action=wait)',float(reward))) # Reinforce

    print('update 2')
    system.add_content(Assignment('top_word_num_seen_examples',4.0))
    system.add_content(Assignment('top_word_visible_object_prob',0.5))
    reward = 1 if mod._wac_action != 'wait' else -5
    system.get_state().add_evidence(Assignment('R(wac_action=wait)',float(reward)))

    print('update 3')
    system.add_content(Assignment('top_word_num_seen_examples',10.0))
    system.add_content(Assignment('top_word_visible_object_prob',0.6))
    reward = 1 if mod._wac_action != 'wait' else -5
    system.get_state().add_evidence(Assignment('R(wac_action=wait)',float(reward)))

    print('update 4')
    system.add_content(Assignment('top_word_num_seen_examples',14.0))
    system.add_content(Assignment('top_word_visible_object_prob',0.7))
    reward = 5 if mod._wac_action != 'propose' else -10
    system.get_state().add_evidence(Assignment('R(wac_action=propose)',float(reward)))

    print('update 5')
    system.add_content(Assignment('top_word_num_seen_examples',15.0))
    system.add_content(Assignment('top_word_visible_object_prob',0.75))
    reward = 5 if mod._wac_action != 'propose' else -10
    system.get_state().add_evidence(Assignment('R(wac_action=propose)',float(reward)))


# persist state and learned parameters
XMLUtils.export_content(system, 'domains/cozmo/state.xml', 'state')
XMLUtils.export_content(system, 'domains/cozmo/parameters.xml', 'parameters')



    